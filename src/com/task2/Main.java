package com.task2;

import com.task2.processor.ListProcessor;
import com.task2.processor.MapProcessor;
import com.task2.processor.SetProcessor;
import com.task2.processor.VerificationSpeed;

public class Main {
    private static int ELEMENTS_COUNT = 100_000;
    private static int DEFAULT_ELEMENT_VALUE = 12341124;
    private static int DEFAULT_ELEMENT_INDEX = 1005;
    private static int KEY = 10;

    public static void main(String[] args) {
        speedVerification(new ListProcessor(ELEMENTS_COUNT));
        speedVerification(new SetProcessor(ELEMENTS_COUNT));
        speedVerification(new MapProcessor(ELEMENTS_COUNT));
    }

    private static void speedVerification(final VerificationSpeed vs) {
        System.out.println(vs.getDefaultMessage());
        System.out.println(vs.comparisonAddingElement(DEFAULT_ELEMENT_VALUE, DEFAULT_ELEMENT_INDEX));
        System.out.println(vs.comparisonFindingElement(DEFAULT_ELEMENT_VALUE, KEY));
        System.out.println(vs.comparisonRemovingElement(DEFAULT_ELEMENT_VALUE, DEFAULT_ELEMENT_INDEX));
    }

}
