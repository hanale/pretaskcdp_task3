package com.task2.processor;

import com.task2.operations.OperationWithList;
import com.task2.operations.Operations;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListProcessor implements VerificationSpeed {
    private Operations arrayList = new OperationWithList(new ArrayList<>());
    private Operations treeList = new OperationWithList(new LinkedList<>());

    public ListProcessor(final int count) {
        arrayList.fill(count);
        treeList.fill(count);
    }

    public String comparisonAddingElement(final int element, final int index) {
        return "Add operation: " + System.lineSeparator() + comparisonAddingElementToList(element, index);
    }

    public String comparisonFindingElement(final int elements, final int key) {
        return "Search operation: " + System.lineSeparator()
                + comparisonFindingElementInList(elements);
    }

    public String comparisonRemovingElement(final int elements, final int index) {
        return "Remove operation: " + System.lineSeparator()
                + comparisonRemovingElementFromList(index);
    }

    public String getDefaultMessage() {
        return System.lineSeparator() + "Operation under ArrayList and LinkedList";
    }

    private String comparisonAddingElementToList(final int element, final int index) {
        long timeUnderArrayList = arrayList.getTimeForAddingElement(element, index);
        long timeUnderTreeList = treeList.getTimeForAddingElement(element, index);
        return getReport(timeUnderArrayList, timeUnderTreeList);
    }

    private String comparisonFindingElementInList(final int element) {
        long timeUnderArrayList = arrayList.getTimeForFindingElement(element);
        long timeUnderTreeList = treeList.getTimeForFindingElement(element);
        return getReport(timeUnderArrayList, timeUnderTreeList);
    }

    private String comparisonRemovingElementFromList(final int index) {
        long timeUnderArrayList = arrayList.getTimeForRemovingElement(index);
        long timeUnderTreeList = treeList.getTimeForRemovingElement(index);
        return getReport(timeUnderArrayList, timeUnderTreeList);
    }

    private boolean isFirstOperationFaster(long operationTime1, long operationTime2) {
        return operationTime1 < operationTime2;
    }

    private String getReport(long timeUnderArrayList, long timeUnderTreeList) {
        if (isFirstOperationFaster(timeUnderArrayList, timeUnderTreeList)) {
            return "Operation under ArrayList is faster than under TreeList (" + timeUnderArrayList + " / "
                    + timeUnderTreeList + ")";
        } else {
            return "Operation under ArrayList is slower than under TreeList (" + timeUnderArrayList + " / "
                    + timeUnderTreeList + ")";
        }
    }
}
