package com.task2.processor;

public interface VerificationSpeed {

    String getDefaultMessage();

    String comparisonAddingElement(final int elements, final int index);

    String comparisonFindingElement(final int elements, final int key);

    String comparisonRemovingElement(final int elements, final int index);

}
