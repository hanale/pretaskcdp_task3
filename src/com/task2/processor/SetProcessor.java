package com.task2.processor;

import com.task2.operations.OperationWithSet;
import com.task2.operations.Operations;

import java.util.HashSet;
import java.util.TreeSet;

public class SetProcessor implements VerificationSpeed {
    /**
     * Because all Set realizations use only values without indexes, but Interface <code>Operations</code>
     * require index <code>OPTIONAL_VALUE</code> uses as default value.
     */
    private int OPTIONAL_VALUE = -1;

    private Operations hashSet = new OperationWithSet(new HashSet<>());
    private Operations treeSet = new OperationWithSet(new TreeSet<>());

    public SetProcessor(final int count) {
        hashSet.fill(count);
        treeSet.fill(count);
    }

    public String comparisonAddingElement(final int elements, final int index) {
        return "Add operation: " + System.lineSeparator() + comparisonAddingElementToSet(elements);
    }

    public String comparisonFindingElement(final int elements, final int key) {
        return "Search operation: " + System.lineSeparator() + comparisonFindingElementInSet(elements);
    }

    public String comparisonRemovingElement(final int elements, final int index) {
        return "Remove operation: " + System.lineSeparator()
                + comparisonRemovingElementFromSet(elements);
    }

    public String getDefaultMessage() {
        return System.lineSeparator() + "Operation under HashSet and TreeSet";
    }

    private String comparisonAddingElementToSet(final int element) {
        long timeUnderHashSet = hashSet.getTimeForAddingElement(element, OPTIONAL_VALUE);
        long timeUnderTreeMap = treeSet.getTimeForAddingElement(element, OPTIONAL_VALUE);
        return getString(timeUnderHashSet, timeUnderTreeMap);
    }

    private String comparisonFindingElementInSet(final int element) {
        long timeUnderHashSet = hashSet.getTimeForFindingElement(element);
        long timeUnderTreeMap = treeSet.getTimeForFindingElement(element);
        return getString(timeUnderHashSet, timeUnderTreeMap);
    }

    private String comparisonRemovingElementFromSet(final int element) {
        long timeUnderHashSet = hashSet.getTimeForRemovingElement(element);
        long timeUnderTreeSet = treeSet.getTimeForRemovingElement(element);
        return getString(timeUnderHashSet, timeUnderTreeSet);
    }

    private static boolean isFirstOperationFaster(long operationTime1, long operationTime2) {
        return operationTime1 < operationTime2;
    }

    private static String getString(long timeUnderHashSet, long timeUnderTreeSet) {
        if (isFirstOperationFaster(timeUnderHashSet, timeUnderTreeSet)) {
            return "Operation under HashSet is faster that under TreeSet (" + timeUnderHashSet + " / "
                    + timeUnderTreeSet + ")";
        } else {
            return "Operation under HashSet is slower that under TreeSet (" + timeUnderHashSet + " / "
                    + timeUnderTreeSet + ")";
        }
    }
}
