package com.task2.processor;


import com.task2.operations.OperationWithMap;
import com.task2.operations.Operations;

import java.util.HashMap;
import java.util.TreeMap;

public class MapProcessor implements VerificationSpeed {
    private Operations hashMap = new OperationWithMap(new HashMap());
    private Operations treeMap = new OperationWithMap(new TreeMap());

    public MapProcessor(final int count) {
        hashMap.fill(count);
        treeMap.fill(count);
    }

    public String comparisonAddingElement(final int elements, final int key) {
        return "Add operation: " + System.lineSeparator()
                + comparisonAddingElementToMap(key, elements);
    }

    public String comparisonFindingElement(final int key, final int elements) {
        return "Search operation: " + System.lineSeparator()
                + comparisonFindingElementInMap(elements);
    }

    public String comparisonRemovingElement(final int elements, final int index) {
        return "Remove operation: " + System.lineSeparator()
                + comparisonRemovingElementFromMap(elements);
    }

    public String getDefaultMessage() {
        return System.lineSeparator() + "Operation under HashMap and TreeMap";
    }

    private String comparisonAddingElementToMap(final int key, final int element) {
        long timeUnderHashMap = hashMap.getTimeForAddingElement(key, element);
        long timeUnderTreeMap = treeMap.getTimeForAddingElement(key, element);
        return getString(timeUnderHashMap, timeUnderTreeMap);
    }

    private String comparisonFindingElementInMap(final int element) {
        long timeUnderHashMap = hashMap.getTimeForFindingElement(element);
        long timeUnderTreeMap = treeMap.getTimeForFindingElement(element);
        return getString(timeUnderHashMap, timeUnderTreeMap);
    }

    private String comparisonRemovingElementFromMap(final int element) {
        long timeUnderHashMap = hashMap.getTimeForRemovingElement(element);
        long timeUnderTreeMap = treeMap.getTimeForRemovingElement(element);
        return getString(timeUnderHashMap, timeUnderTreeMap);
    }

    private boolean isFirstOperationFaster(long operationTime1, long operationTime2) {
        return operationTime1 < operationTime2;
    }

    private String getString(long timeUnderHashMap, long timeUnderTreeMap) {
        if (isFirstOperationFaster(timeUnderHashMap, timeUnderTreeMap)) {
            return "Operation under HashMap is faster that under TreeMap (" + timeUnderHashMap + " / "
                    + timeUnderTreeMap + ")";
        } else {
            return "Operation under HashMap is slower that under TreeMap (" + timeUnderHashMap + " / "
                    + timeUnderTreeMap + ")";
        }
    }
}
