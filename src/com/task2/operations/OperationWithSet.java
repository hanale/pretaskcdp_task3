package com.task2.operations;

import com.task2.counter.ClockCounter;

import java.util.Random;
import java.util.Set;

public class OperationWithSet implements Operations {
    private Set<Integer> set;
    private ClockCounter counter = new ClockCounter();

    public Set getSet() {
        return set;
    }

    public void setSet(final Set<Integer> set) {
        this.set = set;
    }

    public OperationWithSet(final Set<Integer> set) {
        this.set = set;
    }

    public long getTimeForAddingElement(final int element, final int index) {
        counter.start();
        set.add(element);
        return counter.finish();
    }

    public long getTimeForRemovingElement(final int element) {
        counter.start();
        set.remove(element);
        return counter.finish();
    }

    public long getTimeForFindingElement(final int element) {
        counter.start();
        set.contains((element));
        return counter.finish();
    }

    public void fill(final int itemsCount) {
        Random myRandom = new Random();
        for (int i = 0; i < itemsCount; i++) {
            set.add(myRandom.nextInt(100));
        }
    }
}
