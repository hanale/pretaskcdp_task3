package com.task2.operations;

public interface Operations {
    long getTimeForAddingElement(final int element, final int index);

    long getTimeForRemovingElement(final int index);

    long getTimeForFindingElement(final int element);

    void fill(final int itemsCount);
}
