package com.task2.operations;

import com.task2.counter.ClockCounter;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class OperationWithList implements Operations {
    private List<Integer> list;
    private ClockCounter counter = new ClockCounter();

    public List getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public OperationWithList(final List<Integer> list) {
        this.list = list;
    }

    public long getTimeForAddingElement(final int element, final int index) {
        counter.start();
        list.add(index, element);
        return counter.finish();
    }

    public long getTimeForRemovingElement(final int index) {
        counter.start();
        list.remove(index);
        return counter.finish();
    }

    public long getTimeForFindingElement(final int element) {
        counter.start();
        findElement(element);
        return counter.finish();
    }

    private void findElement(final int element) {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(element)) {
                count++;
            }
        }

    }

    public void fill(final int itemsCount) {
        Random myRandom = new Random();
        for (int i = 0; i < itemsCount; i++) {
            list.add(myRandom.nextInt(100));
        }
        // System.out.println("Collection is filled in.");
    }
}
