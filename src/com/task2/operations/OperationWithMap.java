package com.task2.operations;

import com.task2.counter.ClockCounter;

import java.util.Map;
import java.util.Random;

public class OperationWithMap implements Operations {
    private Map<Integer, Integer> map;
    private ClockCounter counter = new ClockCounter();

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public OperationWithMap(Map map) {

        this.map = map;
    }

    public long getTimeForAddingElement(final int key, final int element) {
        counter.start();
        map.put(key, element);
        return counter.finish();
    }

    public long getTimeForRemovingElement(final int element) {
        counter.start();
        map.remove(element);
        return counter.finish();
    }

    public long getTimeForFindingElement(final int element) {
        counter.start();
        map.containsValue(element);
        return counter.finish();
    }

    public void fill(final int itemsCount) {
        Random myRandom = new Random();
        for (int i = 0; i < itemsCount; i++) {
            map.put(i, myRandom.nextInt(100));
        }
    }
}
