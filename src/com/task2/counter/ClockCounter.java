package com.task2.counter;

public class ClockCounter {
    private long start;

    public void start() {
        start = System.nanoTime();
    }

    public long finish() {
        long finish = System.nanoTime();
        return finish - start;
    }
}
